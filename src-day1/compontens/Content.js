import React, { Component } from 'react'

export class Content extends Component {
  render() {
    const { content } = this.props
    return (
      <div>{content}</div>
    )
  }
}

export default Content