import React,{Component} from "react"
import PropTypes from 'prop-types'

// export default function Holle({ aa }){
// 	console.log(aa)
// 	return (
// 		<div>
// 		 {aa}
// 		</div>
// 	)
// }

class Holle extends Component{
	static defaultProps = {
		heigeht: 1.7
	}
	constructor(props){
		super(props)
		this.state = {
			count: 0
		}
		// this.handle = this.handle.bind(this)
	}
	// state = {
	// 	count: 0
	// }

	// handle(){
	// 	console.log(8888)
	// 	console.log(this)
	// }
	// handle = ()=>{
	// 	console.log("this:", this)
	// }
	handle(msg){
		console.log("msg:",msg)
	}
	// handle = (msg)=>{
	// 	console.log("参数：", msg)
	// }
	render(){
		const { aa,heigeht } = this.props
		const { count } = this.state
		return <div>
			{ aa }
			我的身高{ heigeht }
			<hr></hr>
			{ count }
			<hr/>
			{/* <button onClick={ this.handle }> 按钮 </button> */}
			{/* <button onClick={ this.handle.bind(this) }> 按钮 </button> */}
			{/* <button onClick={ this.handle }> 按钮 </button> */}

			{/* <button onClick={ this.handle.bind(this,20) }> 按钮 </button> */}
			{/* <button onClick={ ()=>{ this.handle(10) } }> 按钮 </button> */}

		</div>
	}
}

Holle.propTypes = {
	aa: PropTypes.string
}

export default Holle;