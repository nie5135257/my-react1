import React, { Component } from 'react'

export class Ttl extends Component {
    
  render() {
    const { ttl } = this.props
    return (
      <div>{ttl}</div>
    )
  }
}

export default Ttl