import React, { Component } from 'react'
import {myMoney} from "../context"

export class Two2 extends Component {
  static contextType = myMoney
  render() {
    return (
      <div>{this.context}</div>
    )
  }
}

export default Two2