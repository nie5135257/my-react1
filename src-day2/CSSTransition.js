import React,{Component} from 'react'
import { CSSTransition } from "react-transition-group"
import "animate.css"


export class LayOut extends Component {
	state = {
		flag: true
	}
	flagFn = ()=>{
		this.setState({
			flag: !this.state.flag
		})
	}
  render() {
	const { flag } = this.state
	return (
	  <div>
		 <button onClick={this.flagFn}>1233</button>
		 <CSSTransition
		 	in={ flag }
			 classNames={{
				 enter: "animate__animated",
				 enterActive: "animate__slideInLeft",
				 exit: "animate__animated",
				 exitActive: "animate__slideOutLeft"
			 }}
			 timeout={3000}
			 unmountOnExit
		 >
			 <p>哈哈哈哈</p>
		 </CSSTransition>
	  </div>
	)
  }
}

export default LayOut