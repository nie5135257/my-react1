import React, { Component } from 'react';
import Hello from './compontens/Hello'

console.log(11)

class LayOut extends Component {
    constructor(props){
        super(props)
        console.log('constructor')
        this.state = {
            count: 0,
            flag: true
        }
    }
    // componentWillMount(){
    //     console.log('准备阶段')
    // }
    UNSAFE_componentWillMount(){
        console.log("准备阶段")
    }
    changeCount = ()=>{
        this.setState({
            count: ++this.state.count
        })
    }
    delHello = ()=>{
        this.setState({
            flag: !this.state.flag
        })
    }
    render() {
         console.log("创建虚拟dom")
         const { count ,flag} = this.state
        return (
            <div>
                <button onClick={this.delHello}>删除hello组件</button>
                <button onClick={this.changeCount}> +++</button>
                {flag && <Hello count={count} />}
            </div>
        );
    }
    componentDidMount(){
        console.log('初始化完毕')
    }
    componentDidCatch(err,info){
        console.log('监听报错吧')
        console.log(err,info)
    }
}

console.log(222)

export default LayOut;