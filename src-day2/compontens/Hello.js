import React, { Component } from 'react';
import ReactDOM from  'react-dom'

class Hello extends Component {
    UNSAFE_componentWillReceiveProps(nextProps){
        console.log('nextProps更新了：',nextProps)
        console.log('this.props：',this.props)
    }
    shouldComponentUpdate(nextProps,nextState){
        console.log("是否更新组件", nextProps.count !== 2)
        return nextProps.count !== 2
    }
    UNSAFE_componentWillUpdate(){
        console.log("更新前准备工作")
    }
    delRoot = ()=>{
        ReactDOM.unmountComponentAtNode(document.querySelector('#root'))
    }
    render() {
        console.log('reder')
        const { count } = this.props
        // throw(new Error('错误滴滴滴'))
        // console.log(a)
        return (
            <div>
                <button onClick={this.delRoot}>删除root</button>
                子组件Hello {count}
            </div>
        );
    }
    componentDidUpdate(){
        console.log('更新完成')
    }

    componentWillUnmount(){
        console.log('啊，我要被卸载了')
    }
}

export default Hello;