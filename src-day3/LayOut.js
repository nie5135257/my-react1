import React, { Component } from 'react';

import Add  from './components/Add';
import List  from './components/List';
import Model from './components/Model';
 
class LayOut extends Component {
    state = {
        show: false,
        type: 'add',
        list: [
            {
                id: 0,
                name: "小红",
                age: "20",
                addr: "杭州余杭区"
            }
        ]
    }
    showModel = index=>{
        console.log(index)
        this.setState({
            show: !this.state.show,
            editIndex: index
        })
    }
    addList = obj=>{
        obj.id = new Date().getTime()
        this.state.list.push(obj)
        this.setState({
            list: this.state.list
        })
        this.showModel()
    }
    delList = index=>{
        
        this.state.list.splice(index,1)
        this.setState({
            list: this.state.list
        })
    }
    editList = obj=>{
        const { list,editIndex } = this.state
        console.log(editIndex)
        if(editIndex == null)
            return
        list[editIndex] = Object.assign(list[editIndex],obj)
        
        this.setState({
            list: list
        })
        this.showModel()
    }
    render() {
        const { show,list,editIndex } = this.state
        return (
            <div>
                <Add showModel={this.showModel}></Add>
                <List showModel={this.showModel} list={list} delList={this.delList}></List>
                
                {show&&<Model show={show} obj={list[editIndex]}  showModel={this.showModel} addList={this.addList} editList={this.editList}></Model>}
                
            </div>
        );
    }
}

export default LayOut;