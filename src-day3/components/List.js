import React, { Component } from 'react';

class list extends Component {
    delList = index=>{
        const { delList } = this.props
        delList(index)
    }
    showModel = index=>{
        const { showModel } = this.props
        showModel(index)
    }
    render() {
        const { list } = this.props
        console.log(list)
        return (
            <div>
                <table width={500} border={1} cellPadding={10} cellSpacing={0} style={{
                    marginTop: "50px"
                }}>
                    <tbody>
                    <tr>
                        <th>姓名</th>
                        <th>年龄</th>
                        <th>地址</th>
                        <th>操作</th>
                    </tr>
                    {list.map((item,index)=>{
                        return (<tr key={item.id}>
                            <td>{item.name}</td>
                            <td>{item.age}</td>
                            <td>{item.addr}</td>
                            <td>
                                <button onClick={()=>{this.showModel(index)}}>修改</button>
                                <button onClick={()=>{this.delList(index)}}>删除</button>
                            </td>
                        </tr>)
                    })}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default list;