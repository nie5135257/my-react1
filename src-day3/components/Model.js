import React, { Component } from 'react';
import { CSSTransition } from 'react-transition-group';
import 'animate.css';

class Model extends Component {
    close = ()=>{
        const { showModel } = this.props
        this.refs.name.value = ""
        this.refs.age.value = ""
        this.refs.addr.value = ""
        showModel()
    }
    addList = ()=>{
        const { addList,obj,editList } = this.props
        let addObj = {
            name: this.refs.name.value,
            age: this.refs.age.value,
            addr: this.refs.addr.value
        }
        console.log(obj)
        if(obj)
            editList(addObj)
        else
            addList(addObj)
    }
    render() {
        const { show,obj } = this.props
        return (
            <div>
                <div style={{
                    width: "100vw",
                    height: "100vh",
                    background: "rgba(0,0,0,0.3)",
                    position: "absolute",
                    top: "0px",
                    left: "0px"
                }}>
                    <CSSTransition
                        in={show}
                        timeout={3000}
                        classNames={{
                            enter: "animate__animated",
                            enterActive: "animate__backInDown",
                            exit: "animate__animated",
                            exitActive: "animate__backOutUp"
                        }}
                        
                    >
                        <div style={{
                            width: "600px",
                            height: "200px",
                            position: 'absolute',
                            left: "0px",
                            right: "0px",
                            top: "0px",
                            bottom: "0px",
                            margin: "auto",
                            background: "white",
                            padding: "20px"
                        }}>
                            <div style={{
                                display: "flex",
                                justifyContent: "flex-end"
                            }}>
                                <button onClick={this.close}>关闭</button>
                            </div>
                            <div>
                                <div style={{
                                    margin: "20px"
                                }}>
                                    <span>姓名：</span>
                                    <input ref='name'></input>
                                </div>
                                <div style={{
                                    margin: "20px"
                                }}>
                                    <span>年龄：</span>
                                    <input ref='age'></input>
                                </div>
                                <div style={{
                                    margin: "20px"
                                }}>
                                    <span>地址：</span>
                                    <input ref='addr'></input>
                                </div>
                            </div>
                            <div style={{
                                display: 'flex',
                                justifyContent: "flex-end"
                            }}>
                                <button onClick={this.close}>取消</button>
                                <button style={{
                                    marginLeft: "10px"
                                }} onClick={this.addList}>{obj&&'修改'||"添加"}</button>
                            </div>
                        </div>
                    </CSSTransition>
                    
                </div>
                
            </div>
        );
    }
    componentDidMount(){
        if(!this.props.obj)
            return;
        const  {obj: {name,age,addr}} = this.props
        this.refs.name.value = name
        this.refs.age.value = age
        this.refs.addr.value = addr
    }
}

export default Model;