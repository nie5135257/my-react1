import React from 'react';
import ReactDOM from 'react-dom';
import LayOut from './LayOut';
import { HashRouter,BrowserRouter } from 'react-router-dom'

ReactDOM.render(
    // <HashRouter>
    //     <LayOut/>
    // </HashRouter>
    <BrowserRouter>
        <LayOut/>
    </BrowserRouter>

,document.getElementById("root"))
