import React,{Suspense} from 'react';
import routes from './routes';
import { Redirect,Route,Switch } from 'react-router-dom'

//! Suspense 转场组件 Loading组件
//! exact表示路由完全匹配
//! Roure表示路由展示
export default function RouterComp(){
    return <Suspense fallback={<div>加载中...</div>}>
        {/* <Redirect from="/" to="/home" exact></Redirect> */}
        <Switch>
            {routes.map((item,index)=> <Route key={index} path={item.path} component={item.component}></Route>)}
        </Switch>
    </Suspense>
}