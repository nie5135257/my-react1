import { lazy } from 'react';



export default [
    {
        path: '/home',
        component: lazy(()=>import('../pages/home'))
    },
    {
        path: '/list',
        component: lazy(()=>import('../pages/list'))
    },
    {
        path: '/shop',
        component: lazy(()=>import('../pages/shop'))
    },
   
    {
        path: '',
        component: lazy(()=>import('../pages/404'))
    }
]