import React, { Component } from 'react';
import RouterComp from './router';

import {NavLink} from 'react-router-dom';

import './LayOut.css'

class LayOut extends Component {
    render() {
        return (
            <div>
                <NavLink to="/home" activeClassName='active'>首页</NavLink>
                <NavLink to="/list" activeClassName='active'>列表</NavLink>
                <RouterComp></RouterComp>
            </div>
        );
    }
}

export default LayOut;