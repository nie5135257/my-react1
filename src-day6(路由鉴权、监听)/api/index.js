//! 提前封装数据请求 
import axios from 'axios'
axios.defaults.baseURL = 'https://www.fastmock.site/mock/40b16900d62b909fadafab584e8a6b0f/test';

//! checktoken封装函数
export function getToken () {
    return axios.get('/getInfo')
}

//! checktoken封装函数
export function getIdentity () {
    return axios.get('/identity')
}

