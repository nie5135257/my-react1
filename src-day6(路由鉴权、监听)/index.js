import React from 'react';
import ReactDOM from 'react-dom';
import LayOut from './LayOut';
import { HashRouter,BrowserRouter } from 'react-router-dom'

import { getToken,getIdentity } from './api'
import { setCookie } from './utils';


getToken().then(res=>{
    console.log(res)
    let { status } = res.data
    console.log(status)
    setCookie('tokenStatus',status,7)
    getIdentity().then(res=>{
        const { root } = res.data
        console.log(root)
        setCookie('root',root,7)
    }).catch(err=>Promise.reject(err))
}).catch(err=>{
    Promise.reject(err)
})

ReactDOM.render(
    // <HashRouter>
    //     <LayOut/>
    // </HashRouter>


    <BrowserRouter>
        <LayOut/>
    </BrowserRouter>

,document.getElementById("root"))
