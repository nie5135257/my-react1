import React, { Component } from 'react';
import routes from '../../router/routes';

class index extends Component {
    render() {
        const { location:{pathname} } = this.props

        let flag = routes.some(item=>item.path == pathname)
        return (
            <div>
               { flag&&"暂无权限"||"404" }
            </div>
        );
    }
}

export default index;