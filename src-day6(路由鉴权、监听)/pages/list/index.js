import React, { Component } from 'react';
import { NavLink, Route } from 'react-router-dom';
import Login from './Login';
import Reg from './Reg';



class index extends Component {
    render() {
        return (
            <div>
                列表页
                <NavLink to="/list/login"> 登录</NavLink>
                <NavLink to="/list/reg"> 注册</NavLink>
                <Route path={"/list/login"} component={Login}></Route>
                <Route path={"/list/reg"} component={Reg}></Route>
            </div>
        );
    }
}

export default index;