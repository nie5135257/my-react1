import React,{Suspense,lazy} from 'react';
import routes from './routes';
import { Redirect,Route,Switch,withRouter } from 'react-router-dom'
import { getCookie, getIdentity } from '../utils';
import Login from '../pages/login';

//! Suspense 转场组件 Loading组件
//! exact表示路由完全匹配
//! Roure表示路由展示
 function RouterComp(props){
    const status = getCookie('tokenStatus')
    const root = getCookie('root')
    console.log("root",root)
    console.log('tokenStatus:',status)
    if(status != 0) return <Route path={'/'} component={Login}></Route>
    return <Suspense fallback={<div>加载中...</div>}>
        {/* <Redirect from="/" to="/home" exact></Redirect> */}
        <Switch>
            {/* {
                root=='admin'
                    ?(routes.map((item,index)=> <Route key={index} path={item.path} component={item.component}></Route>))
                    :(routes.filter(item=>item.root != 'admin').map((item,index)=> <Route key={index} path={item.path} component={item.component}></Route>))
            } */}
            {
                routes.map((item,index)=> <Route 
                    key={index} 
                    path={item.path} 
                    render={()=>{
                        console.log(item.root && item.root != root)
                        if(item.root && item.root != root) 
                            return <div>您的权限不够</div>
                        return <item.component {...props}/>
                    }}></Route>)
            }
        </Switch>
    </Suspense>
}

export default withRouter(RouterComp)