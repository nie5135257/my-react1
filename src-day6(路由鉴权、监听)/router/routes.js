import { lazy } from 'react';



export default [
    {
        path: '/home',
        component: lazy(()=>import('../pages/home'))
    },
    {
        path: '/list',
        component: lazy(()=>import('../pages/list'))
    },
    {
        path: '/shop',
        component: lazy(()=>import('../pages/shop')),
        root: 'admin'
    },
    {
        path: '/login',
        component: lazy(()=>import('../pages/login'))
    },
    {
        path: '',
        component: lazy(()=>import('../pages/404'))
    }
]