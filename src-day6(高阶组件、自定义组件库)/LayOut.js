import React, { Component } from 'react';
import One from './components/One';
import Two from './components/Two';
import { hocComp } from './HOC';
 
import { NButton } from "./react-ndg";

const WithOne = hocComp(One)
const WithTwo = hocComp(Two)

class LayOut extends Component {
    
    fn = ()=>{
        console.log(8888)
    }

    render() {
        // console.log(nButton)
        return (
            <div>
                123
                <One></One>
                <WithOne></WithOne>
                <WithTwo></WithTwo>
                <NButton onClick={this.fn}>5555</NButton>
                <NButton type='red'>5555</NButton>
            </div>
        );
    }
}

export default LayOut;