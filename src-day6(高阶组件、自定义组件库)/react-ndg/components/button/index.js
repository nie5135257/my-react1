import React, { Component } from 'react';
import './index.css'

class nButton extends Component {
    render() {
        const { type, onClick } = this.props
        return <button onClick={onClick} className={'btn '+'btn-'+type}>{this.props.children}</button>;
    }
}

export default nButton;