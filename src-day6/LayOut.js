// import React,{useState, useRef} from 'react';
// import Hello from './components/Hello';
// import _ from 'loadsh'

// function LayOut(props) {
//     const [user,setUser] = useState({
//         name: '小明'
//     })
//     const aa = useRef(null)
//     const bb = useRef(null)
//     function setUserFn(){
//         user.name = '小红'
//         // let newUser = {...user}
//         let newUser = _.cloneDeep(user)
//         // setUser(user) //错误
//         setUser(newUser)
//     }
//     function getP(){
//         console.log(aa)
//     }
//     // function getBB(){
//     //     console.log(bb)
//     //     bb.current.add()
//     // }
//     return (
//         <div>
//             <button onClick={setUserFn}>
//                 设置user
//             </button>
//             <div>{user.name}</div>
//             <button onClick={getP}>
//                 获取p
//             </button>
//             <p ref={aa}>adadda</p>
//             {/* <button onClick={getBB}>
//                 获取bb
//             </button>
//             <Hello ref={bb}></Hello> */}
//             <Hello></Hello>
//         </div>
//     );
// }

// export default LayOut;

import React,{useState,useCallback} from 'react';
import Hello from "./components/Hello";
import { useColor } from './all';


const LayOut = () => {
    const [count,setCount] = useState(0)
    const [val,setVal] = useState("")
    const color = useColor(true)
    let toto = useCallback(()=>{
        console.log('useCallBack')
    },[val])
    return (
        <div>
            <button onClick={()=>{setCount(preCount=>preCount+1)}}>8888</button>
            <p>{count}</p>
            <input type="text" onChange={e=>{setVal(e.target.value)}}></input>
            {val}
            <Hello toto={toto}></Hello>
            <p style={{
                background: color
            }}>背景色</p>
        </div>
    );
};

export default LayOut;