import React,{useDebugValue} from "react";

export function useColor(flag){
    useDebugValue(flag?"red":"blue")
    return flag?"red":"blue"
}