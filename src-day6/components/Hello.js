// import React,{useState,useImperativeHandle,useEffect} from 'react';

// const Hello = (props,ref) => {
//     useEffect(()=>{
//         console.log(888)
//     })
//     const [count,setCount] = useState(0)
//     useImperativeHandle(ref,()=>(
//         {
//             a: 10,
//             add(){
//                 setCount(proCount=>proCount+1)
//             }
//         }
//     ))
//     return (
//         <div>
//             {count}
//         </div>
//     );
// };

// export default React.forwardRef(Hello);

// import React,{useState,useMemo} from 'react';

// const Hello = () => {
//     const [count, setCount] = useState(0)
//     const [list,setList] = useState([1,2,3])
//     function rederList(){
//         console.log(888)
//         return list.map((item,index)=><li key={index}>{item}</li>)
//     }
//     let memoList = useMemo(()=>rederList(),[list])
//     return (
//         <div>
//             <button onClick={()=>{setCount(proCount=>proCount+1)}}>++++</button>
//             <div>{count}</div>
//             <hr></hr>
//             <div>
//                 {/* {rederList()} */}
//                 {memoList}
//             </div>
//         </div>
//     );
// };

// export default Hello;

import React from 'react';

const Hello = ({toto}) => {
    toto()
    console.log(888)
    return (
        <div>
            888888
        </div>
    );
};

export default React.memo(Hello);