import React from "react";
import ReactDOM from 'react-dom';
import LayOut from "./LayOut";
import store from "./store";
import { Provider } from "react-redux";

ReactDOM.render(
    <Provider store={store}>
        <LayOut></LayOut>
    </Provider>
,
document.getElementById("root"))