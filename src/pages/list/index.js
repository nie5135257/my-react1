import React,{useState} from 'react';
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {actions} from './store';


function List({getList}) {
    const [list,setList] = useState([])
    let getListX = (obj)=>{
        getList(obj)
    }
    return (
        <div>
            <button onClick={()=>getListX({a:123})}>获取数据</button>
        </div>
    );
}

export default connect(
    ({list})=>list,
    dispatch=>bindActionCreators(actions,dispatch)
)(List);