import { getToken } from "../../api"
export const initState = {
    list: {
        id: 1,
        task: '任务一'
    }
}

export const actions = {
    getList(payload){
        return async dispatch=>{
            dispatch({
                type: 'GET_LIST',
                payload: await getToken(payload)
            })
        }
    }
}

export function reducer(state=initState,{type,payload}){
    const newState = JSON.parse(JSON.stringify(state))
    switch (type) {
        case 'GET_LIST':
            console.log(payload)
            break;
    
        default:
            return newState
            break;
    }
    return newState
}
