import React, { Component } from 'react';
import { connect } from 'react-redux';
import { actions } from './store';
import { bindActionCreators } from "redux"

//connect是一个高阶组件，它可以帮助我们获取store中的数据，并且可以帮助我们更新页面

class User extends Component {
    state = {
        val: ''
    }
    valChange = e => {
        this.setState({
            val: e.target.value
        })
    }
    keyDownFn = e =>{
        const { addUser } = this.props
        const { val } = this.state
        if(e.keyCode == 13){
            addUser(val)
        }
    }
    render() {
        const { val } = this.state
        const { user} = this.props
        return (
            <div>
                <input onKeyDown={this.keyDownFn} value={ val } onChange={ this.valChange }></input>
                {user.map((item,index)=>{
                    return <div key={index}>{item.task}</div>
                })}
            </div>
        );
    }
}
function mapStateFromProps(store){ //遍历所有store，并将store合并到props中
    return store.userStore
}
function mapDispatchFromProps(dispatch){
    //1、帮助我们获取actions上的方法，并绑定到props中
    //2、帮助我们发送actions中的action方法
    return bindActionCreators(actions,dispatch)
}

export default connect(mapStateFromProps,mapDispatchFromProps)(User);