export const initState = {
    user: [
        {
            id: 1,
            task: '任务一'
        }
    ]
}

export const actions = {
    addUser(payload){ 
        //payload 负载(所有参数一般都用负载代替)
        //如果没有进行数据请求，我们只需要创建动作
        return {
            type: "ADD_USER",
            payload
        }
    }
}

export function reducer(state=initState,{type,payload}){
    const newState = JSON.parse(JSON.stringify(state))
    switch (type) {
        case "ADD_USER":
            newState.user.push({
                id: newState.user.length+1,
                task: payload
            })
            break;
    
        default:
            return newState
            break;
    }
    return newState
}