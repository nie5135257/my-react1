import { combineReducers } from 'redux';
import { reducer as userStore } from '../pages/user/store';
import { reducer as listStore } from '../pages/list/store';

const combineReducer = combineReducers({
    userStore,listStore
})

export default combineReducer