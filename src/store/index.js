//applyMiddleware调用中间件
import { createStore,applyMiddleware } from "redux";

//数据对象引用
import combineReducer from "../rootReducer";

//调试工具引用
import { composeWithDevTools } from 'redux-devtools-extension';

//中间件redux-thunk、 redux-sage、 redux-promise
import thunk from "redux-thunk";

const store = createStore(combineReducer,composeWithDevTools(applyMiddleware(thunk)));

export default store;

